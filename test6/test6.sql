--------------------------------------------------------
--  文件已创建 - 星期五-五月-26-2023   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table DEPARTMENT
--------------------------------------------------------

  CREATE TABLE "HR"."DEPARTMENT" 
   (	"DPAT_ID" NUMBER(6,0), 
	"DPAT_NAME" VARCHAR2(50 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "SYSAUX" ;
--------------------------------------------------------
--  DDL for Table EMPLOYEE
--------------------------------------------------------

  CREATE TABLE "HR"."EMPLOYEE" 
   (	"EMPLOYEE" NUMBER(6,0), 
	"EPLY_NAME" VARCHAR2(50 BYTE), 
	"GENDER" VARCHAR2(10 BYTE), 
	"SALARY" NUMBER(6,0), 
	"DPAT_ID" NUMBER(6,0), 
	"PHONE_NUMBER" VARCHAR2(50 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "SYSAUX" ;
--------------------------------------------------------
--  DDL for Table ORDER_
--------------------------------------------------------

  CREATE TABLE "HR"."ORDER_" 
   (	"ORDER_ID" NUMBER(6,0), 
	"ORDER_DATE" DATE, 
	"ORDER_EPLY" NUMBER(6,0), 
	"TOTALL_PRICE" NUMBER(6,0)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "SYSAUX" ;
--------------------------------------------------------
--  DDL for Table ORDER_DETAIL
--------------------------------------------------------

  CREATE TABLE "HR"."ORDER_DETAIL" 
   (	"DETAIL_ID" NUMBER(6,0), 
	"ORDER_ID" NUMBER(6,0), 
	"PRDC_ID" NUMBER(6,0), 
	"DETAIL_NUM" NUMBER(6,0), 
	"DETAIL_PRICE" VARCHAR2(6 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "SYSAUX" ;
--------------------------------------------------------
--  DDL for Table PRODUCT
--------------------------------------------------------

  CREATE TABLE "HR"."PRODUCT" 
   (	"PRDC_ID" NUMBER(6,0), 
	"PRDC_NAME" VARCHAR2(50 BYTE), 
	"PRDC_NUMBER" NUMBER(6,0), 
	"PRICE" NUMBER(6,0)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "SYSAUX" ;
REM INSERTING into HR.DEPARTMENT
SET DEFINE OFF;
REM INSERTING into HR.EMPLOYEE
SET DEFINE OFF;
REM INSERTING into HR.ORDER_
SET DEFINE OFF;
REM INSERTING into HR.ORDER_DETAIL
SET DEFINE OFF;
REM INSERTING into HR.PRODUCT
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index DEPARTMENT_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "HR"."DEPARTMENT_PK" ON "HR"."DEPARTMENT" ("DPAT_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "SYSAUX" ;
--------------------------------------------------------
--  DDL for Index EMPLOYEE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "HR"."EMPLOYEE_PK" ON "HR"."EMPLOYEE" ("EMPLOYEE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "SYSAUX" ;
--------------------------------------------------------
--  DDL for Index ORDER__PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "HR"."ORDER__PK" ON "HR"."ORDER_" ("ORDER_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "SYSAUX" ;
--------------------------------------------------------
--  DDL for Index ORDER_DETAIL_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "HR"."ORDER_DETAIL_PK" ON "HR"."ORDER_DETAIL" ("DETAIL_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "SYSAUX" ;
--------------------------------------------------------
--  DDL for Index PRODUCT_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "HR"."PRODUCT_PK" ON "HR"."PRODUCT" ("PRDC_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "SYSAUX" ;
--------------------------------------------------------
--  Constraints for Table DEPARTMENT
--------------------------------------------------------

  ALTER TABLE "HR"."DEPARTMENT" MODIFY ("DPAT_ID" NOT NULL ENABLE);
  ALTER TABLE "HR"."DEPARTMENT" ADD CONSTRAINT "DEPARTMENT_PK" PRIMARY KEY ("DPAT_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "SYSAUX"  ENABLE;
--------------------------------------------------------
--  Constraints for Table EMPLOYEE
--------------------------------------------------------

  ALTER TABLE "HR"."EMPLOYEE" MODIFY ("EMPLOYEE" NOT NULL ENABLE);
  ALTER TABLE "HR"."EMPLOYEE" MODIFY ("EPLY_NAME" NOT NULL ENABLE);
  ALTER TABLE "HR"."EMPLOYEE" MODIFY ("GENDER" NOT NULL ENABLE);
  ALTER TABLE "HR"."EMPLOYEE" MODIFY ("SALARY" NOT NULL ENABLE);
  ALTER TABLE "HR"."EMPLOYEE" MODIFY ("DPAT_ID" NOT NULL ENABLE);
  ALTER TABLE "HR"."EMPLOYEE" MODIFY ("PHONE_NUMBER" NOT NULL ENABLE);
  ALTER TABLE "HR"."EMPLOYEE" ADD CONSTRAINT "EMPLOYEE_PK" PRIMARY KEY ("EMPLOYEE")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "SYSAUX"  ENABLE;
--------------------------------------------------------
--  Constraints for Table ORDER_
--------------------------------------------------------

  ALTER TABLE "HR"."ORDER_" MODIFY ("ORDER_ID" NOT NULL ENABLE);
  ALTER TABLE "HR"."ORDER_" MODIFY ("ORDER_DATE" NOT NULL ENABLE);
  ALTER TABLE "HR"."ORDER_" MODIFY ("ORDER_EPLY" NOT NULL ENABLE);
  ALTER TABLE "HR"."ORDER_" MODIFY ("TOTALL_PRICE" NOT NULL ENABLE);
  ALTER TABLE "HR"."ORDER_" ADD CONSTRAINT "ORDER__PK" PRIMARY KEY ("ORDER_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "SYSAUX"  ENABLE;
--------------------------------------------------------
--  Constraints for Table ORDER_DETAIL
--------------------------------------------------------

  ALTER TABLE "HR"."ORDER_DETAIL" MODIFY ("DETAIL_ID" NOT NULL ENABLE);
  ALTER TABLE "HR"."ORDER_DETAIL" MODIFY ("ORDER_ID" NOT NULL ENABLE);
  ALTER TABLE "HR"."ORDER_DETAIL" MODIFY ("PRDC_ID" NOT NULL ENABLE);
  ALTER TABLE "HR"."ORDER_DETAIL" MODIFY ("DETAIL_PRICE" NOT NULL ENABLE);
  ALTER TABLE "HR"."ORDER_DETAIL" ADD CONSTRAINT "ORDER_DETAIL_PK" PRIMARY KEY ("DETAIL_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "SYSAUX"  ENABLE;
--------------------------------------------------------
--  Constraints for Table PRODUCT
--------------------------------------------------------

  ALTER TABLE "HR"."PRODUCT" MODIFY ("PRDC_ID" NOT NULL ENABLE);
  ALTER TABLE "HR"."PRODUCT" MODIFY ("PRDC_NAME" NOT NULL ENABLE);
  ALTER TABLE "HR"."PRODUCT" MODIFY ("PRDC_NUMBER" NOT NULL ENABLE);
  ALTER TABLE "HR"."PRODUCT" MODIFY ("PRICE" NOT NULL ENABLE);
  ALTER TABLE "HR"."PRODUCT" ADD CONSTRAINT "PRODUCT_PK" PRIMARY KEY ("PRDC_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "SYSAUX"  ENABLE;
