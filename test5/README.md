班级：1班       学号：202010414117       姓名：宋明熹

# 实验5：包，过程，函数的用法

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```

## 实验过程

1.创建程序包

![test5-1创建程序包](test5-1创建程序包.png)

2.声明函数

![test5-2声明函数](test5-2声明函数.png)

```sql
CREATE OR REPLACE 
PACKAGE MYPACK AS 
    FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
    PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MYPACK;
```

3.生成包主体结构

![test5-3.1生成包主体结构](test5-3.1生成包主体结构.png)

![test5-3.2包主体](test5-3.2包主体.png)

4.修改自动生成的包主体代码

![test5-4修改包主体](test5-4修改包主体.png)

```sql
CREATE OR REPLACE
PACKAGE BODY MYPACK AS

  FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER AS
  N NUMBER(20,2);
  BEGIN
  SELECT SUM(salary) into N  FROM EMPLOYEES E WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
  RETURN N;
  END Get_SalaryAmount;

  PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER) AS
  LEFTSPACE VARCHAR(2000);
  BEGIN
   --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
  END Get_Employees;

END MYPACK;
```

## 测试

测试函数Get_SalaryAmount()

![test5-5.1测试](test5-5.1测试.png)

```sql
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;
```

测试过程Get_Employees()

![test5-5.2测试](test5-5.2测试.png)

```sql
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ; 
END;
```

