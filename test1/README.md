班级：1班       学号：202010414117       姓名：宋明熹

# 实验1：SQL语句的执行计划分析与优化指导

## 实验目的

  分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

  数据库是pdborcl，用户是sys和hr

## 实验内容

- 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
- 设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。

- 用户hr默认没有统计权限，打开统计信息功能autotrace时要报错，必须要向用户hr授予以下视图的选择权限：

```text
 v_$sesstat, v_$statname 和 v_$session
```

- 权限分配过程

```sql
$ sqlplus sys/123@localhost/pdborcl as sysdba
@$ORACLE_HOME/sqlplus/admin/plustrce.sql
create role plustrace;
GRANT SELECT ON v_$sesstat TO plustrace;
GRANT SELECT ON v_$statname TO plustrace;
GRANT SELECT ON v_$mystat TO plustrace;
GRANT plustrace TO dba WITH ADMIN OPTION;
GRANT plustrace TO hr;
GRANT SELECT ON v_$sql TO hr;
GRANT SELECT ON v_$sql_plan TO hr;
GRANT SELECT ON v_$sql_plan_statistics_all TO hr;
GRANT SELECT ON v_$session TO hr;
GRANT SELECT ON v_$parameter TO hr; 
```

- 查询两个部门('IT'和'Sales')的部门总人数和平均工资。

```SQL

$sqlplus hr/123@localhost/pdborcl

set autotrace on

SELECT d.department_name as "部门名称",count(e.job_id)as "部门总人数",
avg(e.salary)as "平均工资"
from hr.departments d,hr.employees e
where d.manager_id = e.manager_id
and d.department_name in ('IT','Sales')
GROUP BY d.department_name;

输出结果：
部门名称                       部门总人数   平均工资
------------------------------ ---------- ----------
IT					4	4950
Sales					6	8500


执行计划
----------------------------------------------------------
Plan hash value: 537644951

--------------------------------------------------------------------------------
----------------

| Id  | Operation		      | Name	       | Rows  | Bytes | Cost (%
CPU)| Time     |

--------------------------------------------------------------------------------
----------------

|   0 | SELECT STATEMENT	      | 	       |     1 |    23 |     5
(20)| 00:00:01 |

|   1 |  HASH GROUP BY		      | 	       |     1 |    23 |     5
(20)| 00:00:01 |

|   2 |   NESTED LOOPS		      | 	       |     5 |   115 |     4
 (0)| 00:00:01 |

|   3 |    NESTED LOOPS 	      | 	       |     6 |   115 |     4
 (0)| 00:00:01 |

|*  4 |     TABLE ACCESS FULL	      | DEPARTMENTS    |     1 |    15 |     3
 (0)| 00:00:01 |

|*  5 |     INDEX RANGE SCAN	      | EMP_MANAGER_IX |     6 |       |     0
 (0)| 00:00:01 |

|   6 |    TABLE ACCESS BY INDEX ROWID| EMPLOYEES      |     6 |    48 |     1
 (0)| 00:00:01 |

--------------------------------------------------------------------------------
----------------


Predicate Information (identified by operation id):
---------------------------------------------------

   4 - filter("D"."MANAGER_ID" IS NOT NULL AND ("D"."DEPARTMENT_NAME"='IT' OR
	      "D"."DEPARTMENT_NAME"='Sales'))
   5 - access("D"."MANAGER_ID"="E"."MANAGER_ID")

Note
-----
   - this is an adaptive plan


统计信息
----------------------------------------------------------
	 33  recursive calls
	  0  db block gets
	 36  consistent gets
	  0  physical reads
	  0  redo size
	775  bytes sent via SQL*Net to client
	607  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	  1  sorts (memory)
	  0  sorts (disk)
	  2  rows processed



```

