班级：1班       学号：202010414117       姓名：宋明熹

# 实验3：创建分区表

## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。

## 实验内容

- 本实验使用实验2的smx用户创建两张表：订单表(orders)与订单详表(order_details)。
- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
- orders表按订单日期（order_date）设置范围分区。
- order_details表设置引用分区。
- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
- 进行分区与不分区的对比实验。

## 实验参考

- 使用sql-developer软件创建表
- 1.1创建订单表
- ![test3-1.1创建订单表](test3-1.1创建订单表.png)

```sql
CREATE TABLE "SMX"."ORDERS"
(
 order_id NUMBER(9, 0) NOT NULL
 , customer_name VARCHAR2(40 BYTE) NOT NULL 
 , customer_tel VARCHAR2(40 BYTE) NOT NULL 
 , order_date DATE NOT NULL 
 , employee_id NUMBER(6, 0) NOT NULL 
 , discount NUMBER(8, 2) DEFAULT 0 
 , trade_receivable NUMBER(8, 2) DEFAULT 0 
 , CONSTRAINT ORDERS_PK PRIMARY KEY 
  (
    ORDER_ID 
  )
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE (   BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL 

PARTITION BY RANGE (order_date) 
(
 PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (
 TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
 'NLS_CALENDAR=GREGORIAN')) 
 NOLOGGING
 TABLESPACE USERS
 PCTFREE 10 
 INITRANS 1 
 STORAGE 
( 
 INITIAL 8388608 
 NEXT 1048576 
 MINEXTENTS 1 
 MAXEXTENTS UNLIMITED 
 BUFFER_POOL DEFAULT 
) 
NOCOMPRESS NO INMEMORY  
, PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (
TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING
TABLESPACE USERS
, PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (
TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING 
TABLESPACE USERS
);

ALTER TABLE smx.orders ADD PARTITION partition_before_2022
VALUES LESS THAN(
TO_DATE('2022-01-01 00:00:00','YYYY-MM-DD  HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN'))
NOLOGGING 
TABLESPACE USERS;
```

- 1.2创建订单详表
- ![test3-1.2创建订单详表](test3-1.2创建订单详表.png)

```sql
CREATE TABLE "SMX"."ORDER_DETAILS"
(
	id NUMBER(9, 0) NOT NULL 
	, order_id NUMBER(10, 0) NOT NULL
	, product_id VARCHAR2(40 BYTE) NOT NULL 
	, product_num NUMBER(8, 2) NOT NULL 
	, product_price NUMBER(8, 2) NOT NULL 
	, CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY 
  (
    id 
  )
	,CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
	REFERENCES smx.orders  (  order_id   )
	ENABLE
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE ( BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL
PARTITION BY REFERENCE (order_details_fk1);
```

1.3表创建完成

![test3-1.3表创建完成](test3-1.3表创建完成.png)

2.orders.customer_name增加B_Tree索引

```sql
CREATE INDEX SMX.ORDERS_INDEX1 ON SMX.ORDERS (CUSTOMER_NAME ASC);
```

3.设置orders.order_id和order_details.id自增及触发器

![test3-3.1新建序列](test3-3.1新建序列.png)

![test3-3.2增长序列](test3-3.2增长序列.png)

```sql
CREATE SEQUENCE SMX.SEQUENCE_ID INCREMENT BY 1 MAXVALUE 999999999 MINVALUE 1 CACHE 20 ORDER;
```

```sql
CREATE TRIGGER SMX.ORDERS_TRG 
BEFORE INSERT ON SMX.ORDERS 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ORDER_ID IS NULL THEN
      SELECT SEQUENCE_ID.NEXTVAL INTO :NEW.ORDER_ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
```

4.SQL脚本随机生成数据,在orders表中插入40万行数据，order_details表插入200万行数据。

4.1插入orders表

![test3-4.1插入orders表](test3-4.1插入orders表.png)

```sql
DECLARE
v_order_id NUMBER;
BEGIN
FOR i IN 1..400000 LOOP
v_order_id := i;
INSERT INTO SMX.orders
(order_id, customer_name, customer_tel, order_date, employee_id, discount, trade_receivable)
VALUES
(v_order_id, 'Customer ' || TRIM(TO_CHAR(i, '000000')), 'Tel ' || TRIM(TO_CHAR(i, '000000')),
TO_DATE('2015-01-01', 'YYYY-MM-DD') + DBMS_RANDOM.VALUE(1, 2555), 
TRUNC(DBMS_RANDOM.VALUE(1, 1000000)), 
TRUNC(DBMS_RANDOM.VALUE(0, 100)),
TRUNC(DBMS_RANDOM.VALUE(100, 10000))); 
IF MOD(i, 10000) = 0 THEN
COMMIT;
END IF;
END LOOP;
COMMIT;
END;
```

4.2插入订单详表

![test3-4.2插入订单详表](test3-4.2插入订单详表.png)

```sql
DECLARE
v_id NUMBER;
v_order_id NUMBER;
BEGIN
FOR i IN 1..400000 LOOP
v_order_id := i;
FOR j IN 1..5 LOOP
v_id := (i - 1) * 5 + j;
INSERT INTO SMX.order_details
(id, order_id, product_id, product_num, product_price)
VALUES
(v_id, v_order_id, 'Product ' || TRIM(TO_CHAR(j, '000')), TRUNC(DBMS_RANDOM.VALUE(1, 10)), TRUNC(DBMS_RANDOM.VALUE(10, 100)));
END LOOP;
IF MOD(i, 10000) = 0 THEN
COMMIT;
END IF;
END LOOP;
COMMIT;
END;
```

5.向用户SMX授予视图选择权限

![test3-5授予权限](test3-5授予权限.png)

```sql
$ sqlplus sys/123@localhost/pdborcl as sysdba
@$ORACLE_HOME/sqlplus/admin/plustrce.sql
create role plustrace;
GRANT SELECT ON v_$sesstat TO plustrace;
GRANT SELECT ON v_$statname TO plustrace;
GRANT SELECT ON v_$mystat TO plustrace;
GRANT plustrace TO dba WITH ADMIN OPTION;
GRANT plustrace TO SMX;
GRANT SELECT ON v_$sql TO SMX;
GRANT SELECT ON v_$sql_plan TO SMX;
GRANT SELECT ON v_$sql_plan_statistics_all TO SMX;
GRANT SELECT ON v_$session TO SMX;
GRANT SELECT ON v_$parameter TO SMX; 
```

6.查询统计信息

```sql
SET AUTOTRACE ON;
SELECT o.order_id, o.customer_name, o.order_date, od.product_id, od.product_num, od.product_price
FROM SMX.orders o
JOIN SMX.order_details od ON o.order_id = od.order_id
WHERE o.trade_receivable=5000;
```