班级：1班       学号：202010414117       姓名：宋明熹

# 实验6： 基于Oracle数据库的商品销售系统的设计

**1.项目简介**

​	本项目是基于Oracle数据库的商品销售系统设计。文档将严格按照标准化规范进行编写，包括详实、规范的设计理念、程序代码和测试数据。系统表和表空间设计合理，数据建模有序，以保障数据的合理性和读写性能。用户管理权限和分配方案设计正确，对用户进行身份验证,确保系统后台操作的安全性。存储过程和函数是保证数据一致性和操作效率的关键，因此系统需要具有高效的PL/SQL设计方案，从而确保代码质量和执行效率。本项目还将配备备份方案，以保障系统数据的安全性和完整性。

**2.ER模型设计**

本项目共有三个实体，分别是部门，员工，商品。

 部门（DEPARTMENT）包括部门ID（DPAT_ID）和部门名称（DPAT_NAME）

![test6-2.1部门实体模型](test6-2.1部门实体模型.png)

员工（EMPLOYEE）包括员工ID（EPLY_ID），员工姓名（EPLY_NAME），性别（GENDER），联系方式（PHONE_NUMBER），工资（SALARY）。其中，因为员工必须有所属的部门，所以部门ID也属于员工的属性，且此项不能为空。

![test6-2.2员工实体模型](test6-2.2员工实体模型.png)

商品（PRODUCT）包括商品ID（PRDC_ID），商品名称（PRDC_NAME），商品价格（PRICE），商品库存（PRDC_NUMBER）。

![test6-2.3商品实体模型](test6-2.3商品实体模型.png)

员工销售商品，是多对多的关系。

![test6-2.4销售实体联系模型](test6-2.4销售实体联系模型.png)

员工处理订单，订单中包含订单详情，其中包括商品信息。

![test6-2.5订单实体联系模型](test6-2.5订单实体联系模型.png)

**3.数据表设计**

项目表结构

![test6-3.1项目表结构](test6-3.1项目表结构.png)

部门表

![test6-3.2部门表](test6-3.2部门表.png)

商品表

![test6-3.3商品表](test6-3.3商品表.png)

员工表

![test6-3.4员工表](test6-3.4员工表.png)

订单表

![test6-3.5订单表](test6-3.5订单表.png)

订单详表

![test6-3.6订单详表](test6-3.6订单详表.png)

**4.创建表空间及分配用户**

表的结构设计好之后，考虑用户和空间分配问题。我们为系统新建一个用户SMX，在磁盘空间方面，考虑将数据存储在PDB上，这里选择默认的PDBORCL数据库。表空间的初始大小是200M。

![test6-4.1创建表空间](test6-4.1创建表空间.png)

![test6-4.2表空间创建成功](test6-4.2表空间创建成功.png)

```sql
$ sqlplus system/123@pdborcl as sysdba
CREATE TABLESPACE Users02 DATAFILE  
'/home/oracle/app/oracle/oradata/orcl/pdborcl/pdbtest_users02_1.dbf' 
  SIZE 100M AUTOEXTEND ON NEXT 256M MAXSIZE UNLIMITED,
'/home/oracle/app/oracle/oradata/orcl/pdborcl/pdbtest_users02_2.dbf'  
  SIZE 100M AUTOEXTEND ON NEXT 256M MAXSIZE UNLIMITED 
EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;
```

用户创建之后，给用户SMX分配表空间USERS和USERS02的使用配额，再分配角色CONNECT和RESOURCE，以便该用户可以连接数据库。

![test6-4.3授权用户](test6-4.3授权用户.png)

```sql
CREATE USER SMX IDENTIFIED BY 123  
DEFAULT TABLESPACE "USERS" 
TEMPORARY TABLESPACE "TEMP";
-- QUOTAS 
ALTER USER SMX QUOTA UNLIMITED ON USERS;
ALTER USER SMX QUOTA UNLIMITED ON USERS02;
-- ROLES 
GRANT "CONNECT" TO SMX WITH ADMIN OPTION;
GRANT "RESOURCE" TO SMX WITH ADMIN OPTION;
ALTER USER SMX DEFAULT ROLE "CONNECT","RESOURCE";
-- SYSTEM PRIVILEGES 
GRANT CREATE VIEW TO SMX WITH ADMIN OPTION;
```

**5.创建表，约束及索引**

创建部门表

![test6-5.1创建部门表](test6-5.1创建部门表.png)

```sql
  CREATE TABLE "HR"."DEPARTMENT" 
   (	"DPAT_ID" NUMBER(6,0) NOT NULL ENABLE, 
	"DPAT_NAME" VARCHAR2(50 BYTE), 
	 CONSTRAINT "DEPARTMENT_PK" PRIMARY KEY ("DPAT_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "SYSAUX"  ENABLE
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "SYSAUX" ;
```

创建员工表

![test6-5.2创建员工表](test6-5.2创建员工表.png)

```sql
  CREATE TABLE "HR"."EMPLOYEE" 
   (	"EMPLOYEE" NUMBER(6,0) NOT NULL ENABLE, 
	"EPLY_NAME" VARCHAR2(50 BYTE) NOT NULL ENABLE, 
	"GENDER" VARCHAR2(10 BYTE) NOT NULL ENABLE, 
	"SALARY" NUMBER(6,0) NOT NULL ENABLE, 
	"DPAT_ID" NUMBER(6,0) NOT NULL ENABLE, 
	"PHONE_NUMBER" VARCHAR2(50 BYTE) NOT NULL ENABLE, 
	 CONSTRAINT "EMPLOYEE_PK" PRIMARY KEY ("EMPLOYEE")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "SYSAUX"  ENABLE
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "SYSAUX" ;
```

创建商品表

![test6-5.3创建商品表](test6-5.3创建商品表.png)



```sql
  CREATE TABLE "HR"."PRODUCT" 
   (	"PRDC_ID" NUMBER(6,0) NOT NULL ENABLE, 
	"PRDC_NAME" VARCHAR2(50 BYTE) NOT NULL ENABLE, 
	"PRDC_NUMBER" NUMBER(6,0) NOT NULL ENABLE, 
	"PRICE" NUMBER(6,0) NOT NULL ENABLE, 
	 CONSTRAINT "PRODUCT_PK" PRIMARY KEY ("PRDC_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "SYSAUX"  ENABLE
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "SYSAUX" ;
```

创建订单表

![test6-5.4创建订单表](test6-5.4创建订单表.png)

```sql
  CREATE TABLE "HR"."ORDER_" 
   (	"ORDER_ID" NUMBER(6,0) NOT NULL ENABLE, 
	"ORDER_DATE" DATE NOT NULL ENABLE, 
	"ORDER_EPLY" NUMBER(6,0) NOT NULL ENABLE, 
	"TOTALL_PRICE" NUMBER(6,0) NOT NULL ENABLE, 
	 CONSTRAINT "ORDER__PK" PRIMARY KEY ("ORDER_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "SYSAUX"  ENABLE
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "SYSAUX" ;
```

创建订单详表

![test6-5.5创建订单详表](test6-5.5创建订单详表.png)



```sql
  CREATE TABLE "HR"."ORDER_DETAIL" 
   (	"DETAIL_ID" NUMBER(6,0) NOT NULL ENABLE, 
	"ORDER_ID" NUMBER(6,0) NOT NULL ENABLE, 
	"PRDC_ID" NUMBER(6,0) NOT NULL ENABLE, 
	"DETAIL_NUM" NUMBER(6,0), 
	"DETAIL_PRICE" VARCHAR2(6 BYTE) NOT NULL ENABLE, 
	 CONSTRAINT "ORDER_DETAIL_PK" PRIMARY KEY ("DETAIL_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "SYSAUX"  ENABLE
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "SYSAUX" ;
```

创建触发器

![test6-5.6触发器1](test6-5.6触发器1.png)

```sql
CREATE OR REPLACE EDITIONABLE TRIGGER "ORDERS_TRIG_ROW_LEVEL"  
BEFORE INSERT OR UPDATE OF DISCOUNT ON "ORDERS"  
FOR EACH ROW --行级触发器
declare 
  m number(8,2);
BEGIN 
  if inserting then 
:new.TRADE_RECEIVABLE:= -:new.discount;
  else 
SELECT sum(DETAIL_NUM*DETAIL_PRICE)INTO m FROM  
ORDER_DETAILS WHERE  ORDER_ID=:old.ORDER_ID;
if m is null then 
        m:=0;
end if;
:new.TRADE_RECEIVABLE:= m -:new.discount;
  end if;
END;
```

![test6-5.7触发器2](test6-5.7触发器2.png)

```sql
CREATE OR REPLACE EDITIONABLE TRIGGER "ORDER_DETAILS_ROW_TRIG"  
AFTER DELETE OR INSERT OR UPDATE  ON ORDER_DETAILS  
FOR EACH ROW  
BEGIN 
  --DBMS_OUTPUT.PUT_LINE(:NEW.ORDER_ID);
  IF:NEW.ORDER_ID IS NOT NULL THEN 
MERGE INTO ORDER_ID_TEMP A 
USING (SELECT 1 FROM DUAL)B 
ON (A.ORDER_ID=:NEW.ORDER_ID) 
WHEN NOT MATCHED THEN 
INSERT (ORDER_ID)VALUES(:NEW.ORDER_ID);
  END IF;
  IF:OLD.ORDER_ID IS NOT NULL THEN 
MERGE INTO ORDER_ID_TEMP A 
USING (SELECT 1 FROM DUAL)B 
ON (A.ORDER_ID=:OLD.ORDER_ID) 
WHEN NOT MATCHED THEN 
INSERT (ORDER_ID)VALUES(:OLD.ORDER_ID);
  END IF;
END;
```

```sql
CREATE OR REPLACE EDITIONABLE TRIGGER "ORDER_DETAILS_SNTNS_TRIG"  
AFTER DELETE OR INSERT OR UPDATE ON ORDER_DETAILS  
declare 
  m number(8,2);
BEGIN 
  FOR R IN (SELECT ORDER_ID FROM ORDER_ID_TEMP) 
  LOOP 
--DBMS_OUTPUT.PUT_LINE(R.ORDER_ID);
SELECT sum(PRODUCT_NUM*PRODUCT_PRICE)INTO m FROM ORDER_DETAILS  
WHERE  ORDER_ID=R.ORDER_ID;
if m is null then 
      m:=0;
end if;
UPDATE ORDERS SET TRADE_RECEIVABLE = m - discount  
WHERE ORDER_ID=R.ORDER_ID; 
  END LOOP;
  DELETE FROM ORDER_ID_TEMP; 
END
```

**6.存储过程与函数**

![test6-6.1存储器1](test6-6.1存储器1.png)

![test6-6.2存储器2](test6-6.2存储器2.png)

```sql
create or replace PACKAGE MyPack IS 
   TYPE refcur is ref cursor;
   FUNCTION Get_SaleAmount(V_DEPARTMENT_ID NUMBER)RETURN NUMBER；
   PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
   FUNCTION Get_EmployeeByPage(v_pageidx number,
      v_pagesize number)return refcur;
   PROCEDURE Get_EmployeeByPage_P(v_pageidx number,
      v_pagesize number,rc out refcur);
   PROCEDURE Calc_All_TradeReceivable;
END MyPack;
create or replace PACKAGE BODY MyPack IS 
FUNCTION Get_SaleAmount(V_DEPARTMENT_ID NUMBER)RETURN NUMBER 
    AS 
N NUMBER(20,2); 
BEGIN 
SELECT SUM(O.TRADE_RECEIVABLE)into N   
FROM ORDERS O,EMPLOYEES E 
WHERE O.EMPLOYEE_ID=E.EMPLOYEE_ID AND  
E.DEPARTMENT_ID =V_DEPARTMENT_ID;
RETURN N;
    END;
  
PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER) 
    AS 
LEFTSPACE VARCHAR(2000);
    begin 
LEFTSPACE:=' ';
for v in 
(SELECT LEVEL,EMPLOYEE_ID,NAME,MANAGER_ID FROM employees  
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID  
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID) 
LOOP 
DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4.' ')|| 
V.EMPLOYEE_ID||' '||v.NAME);
END LOOP;
   END;
```

![test6-6.3存储器3](test6-6.3存储器3.png)

```sql
PROCEDURE Calc_All_TradeReceivable 
   IS 
m number(10,2);
   BEGIN 
FOR R IN (SELECT ORDER_ID FROM ORDERS) 
LOOP 
select sum(PRODUCT_NUM*PRODUCT_PRICE)into m from  
ORDER_DETAILS where ORDER_ID=R.ORDER_ID;
if m is null then 
            m:=0;
end if;
update ORDERS SET TRADE_RECEIVABLE=m-DISCOUNT  
WHERE ORDER_ID=R.ORDER_ID;
END LOOP;
  END;
```

**7.数据库备份**

RMAN（Recovery Manager）是一个由Oracle提供的备份和恢复工具，可以管理备份和恢复操作。使用RMAN备份可以保证备份数据的完整性和一致性，并且可以使用压缩技术来减少备份数据的大小。

RMAN备份大致步骤如下：

（1）配置RMAN备份环境

（2）创建RMAN备份脚本文件，包括备份位置、备份类型、备份频率等信息

（3）使用备份脚本来备份数据库

数据库切换到归档日志模式

```sql
$ sqlplus / as sysdba
SQL> SHUTDOWN IMMEDIATE
SQL> STARTUP MOUNT
SQL> ALTER DATABASE ARCHIVELOG;
SQL> ALTER DATABASE OPEN;
```

数据库完全备份

```sql
$ rman target /
RMAN> SHOW ALL;
RMAN> BACKUP DATABASE;
RMAN> LIST BACKUP;

RMAN> host "rm /home/oracle/app/oracle/oradata/orcl/*.dbf";
RMAN> host "rm /home/oracle/app/oracle/oradata/orcl/pdborcl/*.dbf";
RMAN> host "ls /home/oracle/app/oracle/oradata/orcl/*.dbf";
RMAN> host "ls /home/oracle/app/oracle/oradata/orcl/pdborcl/*.dbf";

RMAN> SHUTDOWN IMMEDIATE;
RMAN> SHUTDOWN ABORT;
RMAN> STARTUP MOUNT;
RMAN> RESTORE DATABASE;
RMAN> recover database;
RMAN> ALTER DATABASE OPEN;
RMAN> host "ls /home/oracle/app/oracle/oradata/orcl/*.dbf";
RMAN> host "ls /home/oracle/app/oracle/oradata/orcl/pdborcl/*.dbf";
/home/oracle/app/oracle/oradata/orcl/pdborcl/sysaux01.dbf  /home/oracle/app/oracle/oradata/orcl/pdborcl/undotbs01.dbf
/home/oracle/app/oracle/oradata/orcl/pdborcl/system01.dbf  /home/oracle/app/oracle/oradata/orcl/pdborcl/users01.dbf
/home/oracle/app/oracle/oradata/orcl/pdborcl/temp01.dbf

```

